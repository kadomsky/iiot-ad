# IIoT diagnosis and predictive maintenance system  #

### Goals and scope ###

Industrial IoT (IIoT) and, more specifically, Cyber-Physical Production Systems (CPPS) 
require robust techniques for detecting anomalies 
and root causes in the system. 

The model-based diagnosis is a commonly used approach in which a dynamic process model captures 
spatio-temporal features of the system’s behavior. Because of the infeasibility of precise mathematical 
or expert modeling, algorithms have been developed for learning such models from system observations. 
These algorithms are characterized by high domain-specialization and yield relatively poor performance 
in other use cases.
   
Here, the CPPS data is used, on which existing models have proven ineffective, to:
 
1) Investigate the perspective of applying deep learning approach to constructing a process model 
in such systems. 

    The main idea tn this scenario is to go from models with fixed structure to more universal 
    techniques for learning optimal structure from dynamic observations. 
    The challenges of evaluating dynamic system models of this class are identified, 
    and evaluation criteria are proposed for representative comparison and benchmarking of the models. 
    It is shown that deep learning models provide increase in anomaly detection score but require 
    additional verification of model robustness.

2) Evaluate the innovative state-based modelling approach

    In this approach, a new type of Hybrid Timed Automata (HTA) is learned from analog IIoT data 
    using sequential clustering. 
    Generally in IIoT systems, timed automata provide a highly useful abstraction for diagnosis and control tasks. 
    Applying them requires automaton to be learned in passive online manner using positive samples only. 
    Currently, Hybrid Timed Automata (HTA) support such kind of learning with OTALA algorithm, 
    but they require a train set of discrete events rather than continuous analog time series typically found in IIoT. 
    Recent attempts to cover this gap involve pre-processing observations with a self-organized maps (SOM) 
    and watershed transform, yet again, resulting models have proven ineffective in many real-world systems. 
    Here, incremental model-based clustering (IMCF) is employed to learn timed automaton from analog IIoT data. 
    IMCF is a sequential algorithm that processes observed time-series online and splits them into a sequence of 
    discrete states with either crisp or fuzzy transitions between them. Such transitions are then treated as 
    events required for HTA identification with OTALA. Obtained models are evaluated in a case of IIoT system 
    that has proved to be challenging for existing modelling techniques. Experimental results show 24.9–76.8% increase 
    in model’s performance and suggest that discretizing obtained with IMCF has higher informativeness 
    for HTA identification. Finally, wider perspectives of applying HTA in IIoT are discussed, 
    and remaining principal limitations are identified.

### Modules ###


### Published results ###

* Kadomskyi K. (2020) [Evaluating Deep Learning models for anomaly detection in an industrial transporting system](results/published/Kadomskyi_2020_Evaluating-Deep-Learning-Models-for-Anomaly-Detection-in-an-Industrial-Transporting-System.pdf). 
*CEUR workshop proceedings*. 2021. Vol. 2845. P. 11–21. ISSN 1613-0073. URL: http://ceur-ws.org/Vol-2845/Paper_2.pdf.

* Kadomskyi K. (2021, preprint) [Online method for learning process states in cyber-physical production systems](results/preprint/Kadomskyi_2021_Online-Method-for-Learning-Process-States-In-Cyber-Physical-Production-Systems.pdf). 
*Computer Science and Information Technology (ICCSIT 2021)*: the 14th international conference on. Paris, France, 2021.

* Кадомский К.К. (2021) Применение потоковой кластеризации для идентификации гибридных темпоральных автоматов на аналоговых данных IIoT.
[Using sequential clustering to identify hybrid timed automata from analog IIoT data](results/published/Kadomskyi_2021_Using-Sequential-Clustering-to-Identify-Hybrid-Timed-Automata-from-Analog-IIoT-Data.pdf). 
*Проблемы управления и информатики*. 2021, № 5. С. 39–52. ISSN 1028-0979.

* Кадомский К.К. (2012) [Повышение эффективности инкрементной кластеризации нечетких данных](results/published/Kadomsky_2012_Improving-the-performance-of-incremental-clustering-of-fuzzy-data.pdf). 
*Труды ИПММ НАН Украины*. 2012, Т. 24. С. 124–133. ISSN 1683-4720.

### Contacts ###
* Cyril Kadomsky, cyril.kadomsky@gmail.com